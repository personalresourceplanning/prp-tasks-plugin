/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

class de {

    static DE() {
        const logger = new Logger("DE Tanslation")
        logger.log("Loading GERMAN for GERMANY.")
        return {
            "commons": {
                "save": "Speichern",
                "delete": "Löschen"
            },
            "tasks": {
                "titleTaskPlugin": "Aufgaben",
                "addList": "Liste erstellen",
                "addTask": "Aufgabe erstellen",
                "title": "Titel",
                "startDate": "Startdatum",
                "dueDate": "Fälligkeitsdatum",
                "duration": "Dauer",
                "description": "Beschreibung",
                "list": "Liste",
                "errorTextTaskEndDate": "Das Startdatum muss vor dem Fälligkeitsdatum liegen!",
                "errorTextListTitle": "Bitte einen Titel eingeben!",
                "noTitle": "[kein Titel]",
                "createTask": "Aufgabe erstellen",
                "createList": "Liste erstellen",
                "wantToDeleteAList": "Liste wirklich löschen?"


            }
        };
    }

}
