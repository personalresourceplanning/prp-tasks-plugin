/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

class TasksController {

    constructor(model, translationLoader, taskService) {
        this.model = model;
        this.trans = translationLoader;
        this.service = taskService;
    }

    toggleTaskAsDone(taskId, isChecked) {
        if (!isChecked) {
            this.service.markAsUndone(taskId);
        } else {
            this.service.markAsDone(taskId);
        }
    }

    deleteTask(taskId) {
        this.service.deleteTask(taskId);
    }

    async deleteTaskFromDetailPage(taskId) {
        await this.service.deleteTask(taskId);
        window.location.href = "#/tasks"
    }

    deleteTaskList(taskListId) {
        this.service.deleteTaskList(taskListId);
    }

    changeTaskListTitle(taskListId, newTitle) {
        this.service.editTaskList(taskListId, newTitle);
    }

    async getTasksDone(taskListId) {
        let taskList = await this.model.getTaskList(taskListId);
        return this.model.getUndoneTasks(taskList.tasks).length;
    }

    async getTasksTotal(taskListId) {
        let taskList = await this.model.getTaskList(taskListId);
        return taskList.tasks.length;
    }

    async createTask(event) {
        event.preventDefault();
        let startDate = new Date($("#task-start-date")[0].value);
        let endDate = new Date($("#task-end-date")[0].value);
        if (startDate.getTime() > endDate.getTime()) {
            $("#task-end-date").addClass("error-input");
            $("#error-text-task-end-date")[0].hidden = false;

        } else {

            $('#createTaskModal').modal('hide');

            let taskTitle = $("#task-title")[0].value;
            if (taskTitle == "") {
                taskTitle = this.trans.tasks.noTitle;
            }

            let startDate = $("#task-start-date")[0].value;
            if (startDate !== "") {
                startDate = new Date(startDate);
                startDate = moment(startDate).format('DD-MM-YYYY HH:mm');
            }

            let endDate = $("#task-end-date")[0].value;
            if (endDate !== "") {
                endDate = new Date(endDate);
                endDate = moment(endDate).format('DD-MM-YYYY HH:mm');
            }

            let task = {
                "title": taskTitle,
                "description": $("#task-description")[0].value,
                "dueDate": endDate,
                "startDate": startDate,
                "duration": $("#selected-task-duration")[0].value
            }

            let taskListId = $("#selected-task-list")[0].value;

            //call server
            await this.service.addTask(task, taskListId);

            //reset Form
            document.createTaskForm.reset();

            //reset error message if necessary
            if ($("#error-text-task-end-date")[0].hidden == false) {
                $("#error-text-task-end-date")[0].hidden = true;
                $("#task-end-date").removeClass("error-input");
            }

            window.location.reload();
        }
    }

    async editTask() {
        let startDate = new Date($("#detail-page-task-start-date")[0].value);
        let endDate = new Date($("#detail-page-task-end-date")[0].value);
        if (startDate.getTime() > endDate.getTime()) {

            $("#detail-page-task-end-date").addClass("error-input");
            $("#detail-page-error-text-task-end-date")[0].hidden = false;

        } else {

            let taskId = $("#detail-page-task-id-span")[0].dataset.taskId;

            let taskTitle = $("#detail-page-task-title")[0].value;
            if (taskTitle == "") {
                taskTitle = this.trans.tasks.noTitle;
            }

            let startDate = $("#detail-page-task-start-date")[0].value;
            if (startDate !== "") {
                startDate = new Date(startDate);
                startDate = moment(startDate).format('DD-MM-YYYY HH:mm');
            }

            let endDate = $("#detail-page-task-end-date")[0].value;
            if (endDate !== "") {
                endDate = new Date(endDate);
                endDate = moment(endDate).format('DD-MM-YYYY HH:mm');
            }

            let task = {
                "id": taskId,
                "title": taskTitle,
                "description": $("#detail-page-task-description")[0].value,
                "dueDate": endDate,
                "startDate": startDate,
                "duration": $("#detail-page-selected-task-duration")[0].value,
                "done": $("#detail-page-task-done")[0].checked,
                "taskListId": $("#detail-page-selected-task-list")[0].value
            }

            //call server
            await this.service.editTask(task);

            //reset Form
            document.detailPageCreateTaskForm.reset();

            //reset error message if necessary
            if ($("#detail-page-error-text-task-end-date")[0].hidden == false) {
                $("#detail-page-error-text-task-end-date")[0].hidden = true;
                $("#detail-page-task-end-date").removeClass("error-input");
            }

            window.location.href = "#/tasks"
        }
    }

    async createList() {
        let listTitle = $("#list-title")[0].value;
        if (listTitle == "") {
            $("#list-title").addClass("error-input");
            $("#error-text-list-title")[0].hidden = false;
        } else {
            $('#createListModal').modal('hide');

            //call Server
            await this.service.addTaskList(listTitle);

            //reset form
            document.createListForm.reset();

            //reset error message if necessary
            if ($("#error-text-list-title")[0].hidden == false) {
                $("#error-text-list-title")[0].hidden = true;
                $("#list-title").removeClass("error-input");
            }

            location.reload();
        }

    }

}
