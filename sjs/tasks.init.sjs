/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

let translationLoader = new TranslationLoader();
translationLoader = translationLoader.loadTranslation();
let view = new TasksView(translationLoader);


switch (window.location.href.split('#')[1].split('?')[0]) {
    case "/tasks":
        view.createContent();
        break;
    case "/tasks/detailpage":
        let id = window.location.href.split('#')[1].split('?')[1].split('=')[1]
        view.createDetailPageTask(id);
        break;
    default:
        break;
}
