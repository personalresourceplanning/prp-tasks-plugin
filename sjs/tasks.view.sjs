/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

class TasksView {

    constructor(translationLoader) {
        this.service = new TasksService();
        this.model = new TasksModel(this.service);
        this.controller = new TasksController(this.model, translationLoader, this.service);
        this.trans = translationLoader;
    }

    async createContent() {
        //set title
        $("#title-task-plugin")[0].textContent = this.trans.tasks.titleTaskPlugin;
        //set text of Plus Button
        this.setTextOfPlusButton();
        //load content
        await this.createTaskListOverview();
        this.createListModal();
        await this.createTaskModal();
        this.deleteTaskListModal(1);
    }

    setTextOfPlusButton() {
        $("#create-list-plus-button")[0].textContent = this.trans.tasks.createList;
        $("#create-task-plus-button")[0].textContent = this.trans.tasks.createTask;
    }

    //initilize createListModal
    createListModal() {
        let createListModalTemplate = $("#modal-template")[0].content;

        let createListModalTemplateCopy = document.importNode(createListModalTemplate, true);

        createListModalTemplateCopy.querySelector('.modal').setAttribute('id', 'createListModal');
        createListModalTemplateCopy.querySelector('.modal-title').textContent = this.trans.tasks.addList;
        createListModalTemplateCopy.querySelector('.btn').textContent = this.trans.commons.save;

        createListModalTemplateCopy.querySelector('.btn').addEventListener("click", this.controller.createList.bind(this.controller));

        let createListForm = $("#create-list-form-template")[0].content;
        let copyCreateListForm = document.importNode(createListForm, true);

        copyCreateListForm.querySelector('[for=list-title]').textContent = this.trans.tasks.title + ":";
        copyCreateListForm.querySelector('#error-text-list-title').textContent = this.trans.tasks.errorTextListTitle;


        createListModalTemplateCopy.querySelector('.modal-body').appendChild(copyCreateListForm);
        $("#task-content")[0].appendChild(createListModalTemplateCopy);
    }

    //initilize createTaskModal
    async createTaskModal() {
        let createTaskModalTemplate = $("#modal-template")[0].content;

        let createTaskModalTemplateCopy = document.importNode(createTaskModalTemplate, true);

        createTaskModalTemplateCopy.querySelector('.modal').setAttribute('id', 'createTaskModal');
        createTaskModalTemplateCopy.querySelector('.modal-title').textContent = this.trans.tasks.addTask;
        createTaskModalTemplateCopy.querySelector('.btn').textContent = this.trans.commons.save;
        createTaskModalTemplateCopy.querySelector('.btn').addEventListener("click", this.controller.createTask.bind(this.controller));

        let createTaskForm = $("#create-task-form-template")[0].content;
        let copyCreateTaskForm = document.importNode(createTaskForm, true);

        //set texts
        copyCreateTaskForm.querySelector('[for=task-title]').textContent = this.trans.tasks.title + ":";
        copyCreateTaskForm.querySelector('[for=task-start-date]').textContent = this.trans.tasks.startDate + ":";
        copyCreateTaskForm.querySelector('[for=selected-task-duration]').textContent = this.trans.tasks.duration + ":";
        copyCreateTaskForm.querySelector('[for=task-end-date]').textContent = this.trans.tasks.dueDate + ":";
        copyCreateTaskForm.querySelector('[for=selected-task-list]').textContent = this.trans.tasks.list + ":";
        copyCreateTaskForm.querySelector('[for=task-description]').textContent = this.trans.tasks.description + ":";
        copyCreateTaskForm.querySelector('#error-text-task-end-date').textContent = this.trans.tasks.errorTextTaskEndDate;


        createTaskModalTemplateCopy.querySelector('.modal-body').appendChild(copyCreateTaskForm);

        $("#task-content")[0].appendChild(createTaskModalTemplateCopy);

        $("#task-start-date")[0].min = moment().format('YYYY-MM-DDTHH:mm');

        await this.createListSelect($("#selected-task-list")[0]);

        this.createTaskDurationSelect($("#selected-task-duration")[0]);

        //add events
        $("#task-start-date")[0].addEventListener("change", this.setMinEndDate);
        $("#task-end-date")[0].addEventListener("change", this.setMaxStartDate);
    }

    deleteTaskListModal(taskListId) {
        let deleteTaskListModalTemplate = $("#modal-template")[0].content;
        let deleteTaskListTemplateCopy = document.importNode(deleteTaskListModalTemplate, true);

        deleteTaskListTemplateCopy.querySelector('.modal').setAttribute('id', `deleteListModal`);
        deleteTaskListTemplateCopy.querySelector('.modal-title').textContent = this.trans.tasks.wantToDeleteAList;
        deleteTaskListTemplateCopy.querySelector('.btn').textContent = this.trans.commons.delete;
        deleteTaskListTemplateCopy.querySelector('.modal-body').remove();

        $("#task-content")[0].appendChild(deleteTaskListTemplateCopy);

    }

//initilize selectedTaskDuration options
    createTaskDurationSelect(select) {

        let option = new Option("", "");
        select.add(option, undefined);

        let minutes = 15;
        while (minutes < 1440) {
            let duration = moment.utc(moment.duration(minutes, "minutes").asMilliseconds()).format("HH:mm");
            let option = new Option(duration, duration);
            select.add(option, undefined);
            minutes = minutes + 15;
        }

    }

    async createListSelect(select) {
        let lists = await this.model.getTaskLists();

        for (let i = 0; i < lists.length; i++) {
            let option = new Option(lists[i].taskListTitle, lists[i].taskListId);
            select.add(option, undefined);
        }
    }

    setMinEndDate() {
        $("#task-end-date")[0].min = $("#task-start-date")[0].value;
    }

    setMaxStartDate() {
        $("#task-start-date")[0].max = $("#task-end-date")[0].value;
    }

    //initilize detail page task
    async createDetailPageTask(taskId) {
        let task = await this.model.getTask(taskId);

        //create copy of template
        let detailPageTaskTemplate = $("#detail-page-task-form-template")[0].content;
        let detailPageTaskTemplateCopy = document.importNode(detailPageTaskTemplate, true);

        //set texts detail-page-title
        detailPageTaskTemplateCopy.querySelector('#detail-page-title').textContent = task.taskTitle;
        detailPageTaskTemplateCopy.querySelector('[for=detail-page-task-start-date]').textContent = this.trans.tasks.startDate + ":";
        detailPageTaskTemplateCopy.querySelector('[for=detail-page-selected-task-duration]').textContent = this.trans.tasks.duration + ":";
        detailPageTaskTemplateCopy.querySelector('[for=detail-page-task-end-date]').textContent = this.trans.tasks.dueDate + ":";
        detailPageTaskTemplateCopy.querySelector('[for=detail-page-selected-task-list]').textContent = this.trans.tasks.list + ":";
        detailPageTaskTemplateCopy.querySelector('[for=detail-page-task-description]').textContent = this.trans.tasks.description + ":";
        detailPageTaskTemplateCopy.querySelector('#detail-page-error-text-task-end-date').textContent = this.trans.tasks.errorTextTaskEndDate;
        detailPageTaskTemplateCopy.querySelector('#detail-page-save-task-button').textContent = this.trans.commons.save;
        detailPageTaskTemplateCopy.querySelector('#detail-page-delete-task-button').textContent = this.trans.commons.delete;


        //set information
        detailPageTaskTemplateCopy.querySelector('#detail-page-task-id-span').dataset.taskId = taskId;
        detailPageTaskTemplateCopy.querySelector('#detail-page-task-done').checked = task.isMarkedAsDone;
        detailPageTaskTemplateCopy.querySelector('#detail-page-task-title').setAttribute('value', task.taskTitle);
        if (task.startDate !== null) {
            detailPageTaskTemplateCopy.querySelector('#detail-page-task-start-date').setAttribute('value', moment(new Date(task.startDate)).format('YYYY-MM-DDTHH:mm:ss'));
        }
        if (task.dueDate !== null) {
            detailPageTaskTemplateCopy.querySelector('#detail-page-task-end-date').setAttribute('value', moment(new Date(task.dueDate)).format('YYYY-MM-DDTHH:mm:ss'));
        }
        detailPageTaskTemplateCopy.querySelector('#detail-page-task-description').textContent = task.description;
        detailPageTaskTemplateCopy.querySelector("#detail-page-delete-task-button").addEventListener("click", this.controller.deleteTaskFromDetailPage.bind(this.controller, taskId));
        detailPageTaskTemplateCopy.querySelector("#detail-page-save-task-button").addEventListener("click", this.controller.editTask.bind(this.controller));


        //create options select task duration
        let selectTaskDuration = detailPageTaskTemplateCopy.querySelector('#detail-page-selected-task-duration');
        this.createTaskDurationSelect(selectTaskDuration);
        //set selected option
        selectTaskDuration.value = moment.utc(moment.duration(task.duration).asMilliseconds()).format("HH:mm");

        //set select task lists
        let selectLists = detailPageTaskTemplateCopy.querySelector('#detail-page-selected-task-list');
        await this.createListSelect(selectLists);
        selectLists.value = task.taskListId;

        //append detail page of task
        $("#task-plugin-div")[0].appendChild(detailPageTaskTemplateCopy);
    }

    createTaskItem(task, taskListId) {

        let taskItemTemplate = $("#task-item-template")[0].content;
        let templateCopy = document.importNode(taskItemTemplate, true);

        //id
        templateCopy.querySelector(".list-group-item").setAttribute("id", `task${task.taskId}`)

        // set marking function
        templateCopy.querySelector(".task-item-check-box").addEventListener("click", this.markTaskAsDone.bind(this, task.taskId, taskListId));
        // marked as done
        templateCopy.querySelector(".task-item-check-box").checked = task.isMarkedAsDone;
        //Title
        templateCopy.querySelector(".task-item-title").textContent = task.taskTitle;
        if (task.isMarkedAsDone) {
            this.toggleMarkASDone(task.taskId);
        }
        //Link
        templateCopy.querySelector(".task-item-title").setAttribute("href", task.taskLink);

        // set Delete function
        templateCopy.querySelector(".delete-task-button").addEventListener("click", this.deleteTask.bind(this, task.taskId, taskListId));


        $(`#list${taskListId}`)[0].appendChild(templateCopy);

    }

    createTaskListItem(taskList) {

        let taskListTemplate = $("#task-list-template")[0].content;
        let templateCopy = document.importNode(taskListTemplate, true);

        // set Id listHeader
        templateCopy.querySelector(".task-list-header").setAttribute("id", `task-list-header${taskList.taskListId}`);


        // Title
        templateCopy.querySelector(".task-list-title").textContent = taskList.taskListTitle;
        templateCopy.querySelector(".task-list-title").addEventListener("focusout", this.changeTaskListTitle.bind(this, taskList.taskListId));
        templateCopy.querySelector(".task-list-title").addEventListener("focus", this.getOldTaskListTitle.bind(this, taskList.taskListId));

        // Remaining subtasks
        templateCopy.querySelector(".subtasks-done").textContent = `${taskList.subTasksDone}`;
        templateCopy.querySelector(".subtasks-total").textContent = `${taskList.subTasksTotal}`;

        // set Id list
        templateCopy.querySelector(".list-group").setAttribute("id", `list${taskList.taskListId}`);
        // set collapse
        templateCopy.querySelector(".collapse-list-button").setAttribute("data-target", `#collapse${taskList.taskListId}`);
        templateCopy.querySelector(".collapse-list-title").setAttribute("data-target", `#collapse${taskList.taskListId}`);
        templateCopy.querySelector(".panel-collapse").setAttribute("id", `collapse${taskList.taskListId}`);
        // set Delete function
        templateCopy.querySelector(".delete-task-list-button").setAttribute("data-target", `#deleteListModal`);

        templateCopy.querySelector(".delete-task-list-button").addEventListener("click", this.updateDeleteTaskListModal.bind(this, taskList.taskListId));


        $("#lists")[0].appendChild(templateCopy);

        for (let i = 0, len = taskList.tasks.length; i < len; i++) {
            this.createTaskItem(taskList.tasks[i], taskList.taskListId);
        }
    }

    updateDeleteTaskListModal(taskListId) {
        let deleteTaskListTemplate = $("#deleteListModal")[0];
        let deleteButton = deleteTaskListTemplate.querySelector('.btn');
        // remove old event listener
        deleteButton.replaceWith(deleteButton.cloneNode(true));
        // add new event listener with tasklist id
        deleteTaskListTemplate.querySelector('.btn').addEventListener("click", this.deleteTaskList.bind(this, taskListId));
    }

    async markTaskAsDone(taskId, taskListId) {
        let isChecked = $(`#task${taskId}`)[0].querySelector(".task-item-check-box").checked;
        this.controller.toggleTaskAsDone(taskId, isChecked);
        this.toggleMarkASDone(taskId);
        await this.updateTaskCount(taskListId);
    }

    toggleMarkASDone(taskId) {
        $(document).ready(function () {
            $($(`#task${taskId}`)[0].querySelector(".task-item-title")).toggleClass("marked-as-done");
        });
    }

    async deleteTask(taskId, taskListId) {
        this.controller.deleteTask(taskId);
        $(`#task${taskId}`)[0].remove();
        await this.updateTaskCount(taskListId);
    }

    deleteTaskList(taskListId) {

        this.controller.deleteTaskList(taskListId);

        $(`#task-list-header${taskListId}`)[0].remove();
        $(`#collapse${taskListId}`)[0].remove();

        $(`#deleteListModal`).modal('hide');
    }

    changeTaskListTitle(taskListId) {
        let newTitle = $(`#task-list-header${taskListId}`)[0].querySelector(".task-list-title").textContent;
        let oldTitle = sessionStorage.getItem(`oldTitleOfTaskList${taskListId}`);
        if (newTitle === "") {
            $(`#task-list-header${taskListId}`)[0].querySelector(".task-list-title").textContent = oldTitle;
        } else {
            this.controller.changeTaskListTitle(taskListId, newTitle);
        }
    }

    getOldTaskListTitle(taskListId) {
        let oldTitle = $(`#task-list-header${taskListId}`)[0].querySelector(".task-list-title").textContent;
        sessionStorage.setItem(`oldTitleOfTaskList${taskListId}`, oldTitle);
    }

    async updateTaskCount(taskListId) {
        let subTasksDone = await this.controller.getTasksDone(taskListId);
        let subTasksTotal = await this.controller.getTasksTotal(taskListId);
        let list = $(`#task-list-header${taskListId}`)[0];
        list.querySelector(".subtasks-done").textContent = `${subTasksDone}`;
        list.querySelector(".subtasks-total").textContent = `${subTasksTotal}`;
    }

    async createTaskListOverview() {
        let taskLists = await this.model.getTaskLists();
        for (let i = 0; i < taskLists.length; i++) {
            this.createTaskListItem(taskLists[i]);
        }

        // max Listtitle length
        $("p[contenteditable='true'][maxlength]").on('keydown paste', function (event) {
            let cntMaxLength = parseInt($(this).attr('maxlength'));

            if ($(this).text().length === cntMaxLength && event.keyCode != 8) {
                event.preventDefault();
            }
        });
    }

}
